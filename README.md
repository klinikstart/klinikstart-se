# Klinikstart

[![Netlify Status](https://api.netlify.com/api/v1/badges/0b1ef362-946d-457d-8c9f-68ab3419cddb/deploy-status)](https://app.netlify.com/sites/klinikstart/deploys)

**Klinikstart** är en startsida som samlar användbara resurser för kliniker.
Har du tips på resurser som du använder i din kliniska vardag eller har du hittat en bug 🐛, hjälp till att göra **Klinikstart** bättre genom [öppna en "issue"](https://gitlab.com/alping/klinikstart/issues)!

## Tech Stack

- [Nuxt3](https://v3.nuxtjs.org/)
- [UnoCSS](https://github.com/antfu/unocss)
- [Iconify](https://iconify.design/)
- [Pinia](https://pinia.esm.dev/)
- [VueUse](https://vueuse.org/)
- [Fuse.js](https://fusejs.io/)
- [Nanoid](https://github.com/ai/nanoid)

## Favicon Generation

- Color: #b91c1c
- [Favicon Generator](https://favicon.io/favicon-generator/)
- [Maskable Icons](https://maskable.app/)

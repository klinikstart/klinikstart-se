// Resources
export interface ResourceType {
    id?: number
    name: string
    url: string
    group: string
}

export interface ResourceTypeExtended extends ResourceType {
    active: boolean
    highlight: boolean
    searchIndex: number
}

// Providers
export interface ProviderType {
    key: string
    name: string
    url: string
    searchParam: string
    description: string
    extraParams?: { string: string }
    hidden?: boolean
}

// Akut Medicin
export interface AkutMedicinDataType {
    accessed_time: string
    accessed_url: string
    groups: AkutMedicinGroupType[]
    items: AkutMedicinType[]
}

export interface AkutMedicinGroupType {
    id?: number
    name: string
    url: string
}

export interface AkutMedicinType {
    id?: number
    name: string
    group: string
    url: string
}

// Akut Kardiologi
export interface AkutKardiologiDataType {
    accessed_time: string
    accessed_url: string
    groups: string[]
    items: AkutKardiologiType[]
}

export interface AkutKardiologiType {
    id?: number
    name: string
    group: string
    url: string
}

// Akut Kirurgi
export interface AkutKirurgiDataType {
    accessed_time: string
    accessed_url: string
    parts: string[]
    sections: AkutKirurgiType[]
    items: AkutKirurgiType[]
}

export interface AkutKirurgiType {
    id?: number
    name: string
    section?: string
    part: string
    url: string
}

// Läkartidningens ABC
export interface AbcDataType {
    accessed_time: string
    accessed_url: string
    items: AbcType[]
}

export interface AbcType {
    id?: number
    date: string
    name: string
    url: string
    url_pdf: string
}

// Learning
export interface LearningType {
    id?: number
    name: string
    description: string
    url: string
    availability: 'Gratis' | 'Registrering' | 'Betaltjänst'
}

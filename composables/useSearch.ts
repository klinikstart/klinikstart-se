import { onKeyDown } from "@vueuse/core"

export default function () {
    const searchQuery = ref('')
    const searchFocus = ref(true)

    const clearSearch = () => {
        searchQuery.value = ''
        searchFocus.value = true
    }

    // Focus search on CTRL + K
    onKeyDown('k', e => {
        if (e.ctrlKey) {
            e.preventDefault()
            searchFocus.value = true
        }
    })

    // Clear and focus search on Escape
    onKeyDown('Escape', e => {
        clearSearch()
    })

    return { searchQuery, searchFocus, clearSearch }
}

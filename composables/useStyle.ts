/* @unocss-include */
// https://github.com/antfu/unocss/issues/166

export default function () {
    const kirurgiBgColors = {
        'kirurgi': 'bg-red-500',
        'urologi': 'bg-amber-500',
        'trauma-initialt-omhandertagande': 'bg-teal-500',
        'trauma-specifika-skador': 'bg-teal-500',
        'akut-karlkirurgi': 'bg-rose-500',
        'brost-och-endokrinkirurgi': 'bg-pink-500',
        'barnkirurgi': 'bg-cyan-500',
        'handkirurgi': 'bg-stone-500',
        'oron-nasa-och-hals': 'bg-lime-500',
        'lilla-kirurgin': 'bg-fuchsia-500',
        'sar-och-mjukdelsinfektioner': 'bg-violet-500',
        'appendix': 'bg-gray-700',
        'ovrigt': 'bg-gray-500',
    }

    const kirurgiBorderColors = {
        'kirurgi': 'border-red-500',
        'urologi': 'border-amber-500',
        'trauma-initialt-omhandertagande': 'border-teal-500',
        'trauma-specifika-skador': 'border-teal-500',
        'akut-karlkirurgi': 'border-rose-500',
        'brost-och-endokrinkirurgi': 'border-pink-500',
        'barnkirurgi': 'border-cyan-500',
        'handkirurgi': 'border-stone-500',
        'oron-nasa-och-hals': 'border-lime-500',
        'lilla-kirurgin': 'border-fuchsia-500',
        'sar-och-mjukdelsinfektioner': 'border-violet-500',
        'appendix': 'border-gray-700',
        'ovrigt': 'border-gray-500',
    }

    return { kirurgiBgColors, kirurgiBorderColors }
}

import config from '#config'

let contributors: string[]

export default async () => {
    if (!contributors) {
        try {
            contributors = await $fetch(`${config.DATA_API}/contributors.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    return contributors
}

import config from '#config'
import { ProviderType } from '~/types/dataTypes'

let providers: ProviderType[]

export default async () => {
    if (!providers) {
        try {
            providers = await $fetch(`${config.DATA_API}/providers.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    return providers
}

import config from '#config'
import { AbcDataType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let abc: AbcDataType

export default async () => {
    if (!abc) {
        try {
            abc = await $fetch(`${config.DATA_API}/abcLakartidningen.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    abc.items = addId(abc.items)

    return abc
}

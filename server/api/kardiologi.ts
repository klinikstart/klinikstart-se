import config from '#config'
import { AkutKardiologiDataType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let kardiologi: AkutKardiologiDataType

export default async () => {
    if (!kardiologi) {
        try {
            kardiologi = await $fetch(`${config.DATA_API}/akutKardiologi.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    kardiologi.items = addId(kardiologi.items)

    return kardiologi
}

import config from '#config'
import { AkutKirurgiDataType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let kirurgi: AkutKirurgiDataType

export default async () => {
    if (!kirurgi) {
        try {
            kirurgi = await $fetch(`${config.DATA_API}/akutKirurgi.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    kirurgi.items = addId(kirurgi.items)

    return kirurgi
}

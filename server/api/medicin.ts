import config from '#config'
import { AkutMedicinDataType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let medicin: AkutMedicinDataType

export default async () => {
    if (!medicin) {
        try {
            medicin = await $fetch(`${config.DATA_API}/akutInternmedicin.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    medicin.items = addId(medicin.items)

    return medicin
}

import config from '#config'
import { ResourceType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let resources: ResourceType[]

export default async () => {
    if (!resources) {
        try {
            resources = await $fetch(`${config.DATA_API}/resources.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    resources = addId(resources)

    return resources
}

import config from '#config'
import { LearningType } from '~/types/dataTypes'
import { addId } from '~/utils/utils'

let learning: LearningType[]

export default async () => {
    if (!learning) {
        try {
            learning = await $fetch(`${config.DATA_API}/learning.json`)
        } catch (err) {
            console.error((err as Error).message)
        }
    }

    // Add ID
    learning = addId(learning)

    return learning
}

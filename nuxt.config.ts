import { defineNuxtConfig } from 'nuxt3'
import { version, homepage } from './package.json'

export default defineNuxtConfig({
    meta: {
        htmlAttrs: { lang: 'se' },
        title: "Klinikstart",
        meta: [
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { name: 'theme-color', content: '#b91c1c' },
            {
                hid: 'description',
                name: 'description',
                content: 'En startsida för kliniker.'
            }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'icon', type: 'image/png', sizes: "16x16", href: '/favicon-16x16.png' },
            { rel: 'icon', type: 'image/png', sizes: "32x32", href: '/favicon-32x32.png' },
            { rel: 'apple-touch-icon', type: 'image/png', sizes: "180x180", href: '/apple-touch-icon.png' },
            { rel: 'manifest', type: 'application/manifest+json', href: '/site.webmanifest' },
        ]
    },
    publicRuntimeConfig: {
        VERSION: version,
        BASE_URL: homepage,
        DEPLOY_DATE: new Date(),
    },
    privateRuntimeConfig: {
        DATA_API: 'https://data.klinikstart.se',
    },
    buildModules: [
        '@pinia/nuxt',
        '@unocss/nuxt',
        '@vueuse/nuxt',
        'vue-plausible'
    ],
    vueuse: {
        ssrHandlers: true,
    },
    unocss: {
        uno: true,
        icons: true,
        attributify: true,
        preflight: true,
        theme: {
            colors: {
                'primary': '#B91C1C',
            },
        },
        shortcuts: [
            ['main-padding', 'p3 sm:p5'],
            ['grid-layout', 'grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-3 md:gap-5'],
            ['grid-item', 'p-4 bg-white rounded-sm shadow-sm border-gray-200 border-l-8 md:(border-t-8 border-l-0) hover:border-blue-400 focus:(border-blue-500 outline-none)'],
            ['list-layout', 'flex flex-col space-y-1'],
            ['list-item', 'p-4 bg-white rounded-sm shadow-sm border-gray-200 border-l-8 hover:border-blue-400 focus:(border-blue-500 outline-none)'],
        ],
    },
    plausible: {
        domain: 'klinikstart.se',
        // There seems to be an issue with the redirects in Netlify
        // apiHost: 'https://klinikstart.se/insight',
    },
    vite: {
        logLevel: 'info',
    },
})

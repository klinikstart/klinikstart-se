import { acceptHMRUpdate, defineStore } from 'pinia'

export const useContributorStore = defineStore('contributors', () => {
    const loaded = ref<boolean>(false)
    const items = ref<string[]>([])

    async function loadLearning() {
        const { data, pending } = await useFetch('/api/contributors')
        loaded.value = !pending.value
        items.value = data.value
    }

    return { loaded, items, loadLearning }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useContributorStore, import.meta.hot))

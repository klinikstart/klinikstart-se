import { acceptHMRUpdate, defineStore } from 'pinia'
import { AkutKardiologiType } from '~/types/dataTypes'
import Fuse from 'fuse.js'


export const useKardiologiStore = defineStore('kardiologi', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
    }

    const loaded = ref<boolean>(false)
    const time = ref<string>('')
    const url = ref<string>('')
    const groups = ref<string[]>([])
    const items = ref<AkutKardiologiType[]>([])
    const index = markRaw(new Fuse<AkutKardiologiType>([], searchOptions))

    async function loadKardiologi() {
        const { data, pending } = await useFetch('/api/kardiologi')
        loaded.value = !pending.value
        time.value = data.value.accessed_time
        url.value = data.value.accessed_url
        groups.value = data.value.groups
        items.value = data.value.items
    }

    // Create new search index on item change
    watch(items, () => index.setCollection(items.value))

    return { loaded, time, url, groups, items, index, loadKardiologi }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useKardiologiStore, import.meta.hot))

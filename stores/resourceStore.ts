import { acceptHMRUpdate, defineStore } from 'pinia'
import { ResourceType } from '~/types/dataTypes'
import Fuse from 'fuse.js'

export const useResourceStore = defineStore('resource', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
    }

    const loaded = ref<boolean>(false)
    const resources = ref<ResourceType[]>([])
    const index = markRaw(new Fuse<ResourceType>([], searchOptions))

    async function loadResources() {
        const { data, pending } = await useFetch('/api/resources')
        loaded.value = !pending.value
        resources.value = data.value
    }

    // Create new search index on item change
    watch(resources, () => index.setCollection(resources.value))

    return { loaded, resources, index, loadResources }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useResourceStore, import.meta.hot))

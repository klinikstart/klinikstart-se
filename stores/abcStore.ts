import { acceptHMRUpdate, defineStore } from 'pinia'
import { AbcType } from '~/types/dataTypes'
import Fuse from 'fuse.js'

export const useAbcStore = defineStore('abc', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
    }

    const loaded = ref<boolean>(false)
    const time = ref<string>('')
    const url = ref<string>('')
    const items = ref<AbcType[]>([])
    const index = markRaw(new Fuse<AbcType>([], searchOptions))

    async function loadAbc() {
        const { data, pending } = await useFetch('/api/abc')
        loaded.value = !pending.value
        time.value = data.value.accessed_time
        url.value = data.value.accessed_url
        items.value = data.value.items
    }

    // Create new search index on item change
    watch(items, newItems => { index.setCollection(newItems) })

    return { loaded, time, url, items, index, loadAbc }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useAbcStore, import.meta.hot))

import { acceptHMRUpdate, defineStore } from 'pinia'
import { ProviderType } from '~/types/dataTypes'

export const useProviderStore = defineStore('provider', () => {
    const loaded = ref<boolean>(false)
    const providers = ref<ProviderType[]>([])

    async function loadProviders() {
        const { data, pending } = await useFetch('/api/providers')
        loaded.value = !pending.value
        providers.value = data.value
    }

    return { loaded, providers, loadProviders }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useProviderStore, import.meta.hot))

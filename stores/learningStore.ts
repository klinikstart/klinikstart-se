import { acceptHMRUpdate, defineStore } from 'pinia'
import { LearningType } from '~/types/dataTypes'
import Fuse from 'fuse.js'


export const useLearningStore = defineStore('learning', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
    }

    const loaded = ref<boolean>(false)
    const items = ref<LearningType[]>([])
    const index = markRaw(new Fuse<LearningType>([], searchOptions))

    async function loadLearning() {
        const { data, pending } = await useFetch('/api/learning')
        loaded.value = !pending.value
        items.value = data.value
    }

    // Create new search index on item change
    watch(items, () => index.setCollection(items.value))

    return { loaded, items, index, loadLearning }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useLearningStore, import.meta.hot))

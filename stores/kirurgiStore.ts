import { acceptHMRUpdate, defineStore } from 'pinia'
import { AkutKirurgiType } from '~/types/dataTypes'
import Fuse from 'fuse.js'


export const useKirurgiStore = defineStore('kirurgi', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
        includeScore: true,
    }

    const loaded = ref<boolean>(false)
    const time = ref<string>('')
    const url = ref<string>('')
    const parts = ref<string[]>([])
    const sections = ref<AkutKirurgiType[]>([])
    const items = ref<AkutKirurgiType[]>([])
    const itemIndex = markRaw(new Fuse<AkutKirurgiType>([], searchOptions))
    const sectionIndex = markRaw(new Fuse<AkutKirurgiType>([], searchOptions))

    async function loadKirurgi() {
        const { data, pending } = await useFetch('/api/kirurgi')
        loaded.value = !pending.value
        time.value = data.value.accessed_time
        url.value = data.value.accessed_url
        parts.value = data.value.parts
        sections.value = data.value.sections
        items.value = data.value.items
    }

    // Create new search index on item change
    watch(items, () => itemIndex.setCollection(items.value))
    watch(items, () => sectionIndex.setCollection(sections.value))

    return {
        loaded, time, url, parts, sections, items,
        itemIndex, sectionIndex, loadKirurgi
    }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useKirurgiStore, import.meta.hot))

import { acceptHMRUpdate, defineStore } from 'pinia'
import { AkutMedicinType, AkutMedicinGroupType } from '~/types/dataTypes'
import Fuse from 'fuse.js'


export const useMedicinStore = defineStore('medicin', () => {
    const searchOptions = {
        keys: ['name'],
        threshold: 0.4,
        distance: 100,
    }

    const loaded = ref<boolean>(false)
    const time = ref<string>('')
    const url = ref<string>('')
    const groups = ref<AkutMedicinGroupType[]>([])
    const items = ref<AkutMedicinType[]>([])
    const itemIndex = markRaw(new Fuse<AkutMedicinType>([], searchOptions))
    const groupIndex = markRaw(new Fuse<AkutMedicinGroupType>([], searchOptions))

    async function loadMedicin() {
        const { data, pending } = await useFetch('/api/medicin')
        loaded.value = !pending.value
        time.value = data.value.accessed_time
        url.value = data.value.accessed_url
        groups.value = data.value.groups
        items.value = data.value.items
    }

    // Create new search index on item change
    watch(items, () => itemIndex.setCollection(items.value))
    watch(groups, () => groupIndex.setCollection(groups.value))

    return { loaded, time, url, groups, items, itemIndex, groupIndex, loadMedicin }
})

if (import.meta.hot)
    import.meta.hot.accept(acceptHMRUpdate(useMedicinStore, import.meta.hot))

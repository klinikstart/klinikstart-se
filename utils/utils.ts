import { nanoid } from 'nanoid'
import { ProviderType } from '~/types/dataTypes'

export function normalizeName(name: string) {
    //! Cannot use replaceAll, crashes on reload
    return name.toLowerCase()
        .replace(/ /g, '-')
        .replace(/,/g, '')
        .replace(/-+/g, '-')
        .replace(/å/g, 'a')
        .replace(/ä/g, 'a')
        .replace(/ö/g, 'o')
}

export function addId(items: any) {
    return items.map(item => Object.assign(item, { id: nanoid() }))
}

export function isLocalUrl(url: string | URL) {
    return url.toString().startsWith('/')
}

export function toLocalUrl(baseUrl: string | URL, url: string | URL) {
    return url.toString().startsWith(baseUrl.toString())
        ? url.toString().slice(baseUrl.toString().length)
        : url.toString()
}

export function getProviderBaseUrl(provider: ProviderType) {
    const providerUrl = new URL(provider.url)
    const targetUrl = new URL(providerUrl.protocol + '//' + providerUrl.hostname)
    if ('extraParams' in provider) {
        targetUrl.search = (new URLSearchParams(provider['extraParams'])).toString()
    }
    return targetUrl
}

export function getProviderSearchUrl(provider: ProviderType, query: string): URL {
    const url = new URL(provider['url'])
    var params: URLSearchParams

    // Add optional parameters if they exist
    if ('extraParams' in provider) {
        params = new URLSearchParams(provider['extraParams'])
    } else {
        params = new URLSearchParams()
    }

    // Add search parameter
    params.append(provider.searchParam, query)
    url.search = params.toString()

    return url
}

export function openInNewTab(url: string | URL) {
    const newWindow = window.open(url, '_blank', 'noopener, noreferrer')
    if (newWindow) newWindow.opener = null
}
